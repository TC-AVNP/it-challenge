# IT Challenge

Hey 😄

Welcome to our IT Challenge repository. This README will guide you on how to participate in this challenge.

Please fork this repo before you start working on the challenge. We will evaluate the code on the fork.

FYI: Please understand that this challenge is not decisive if you are applying to work at Unbabel. There are no right and wrong answers. This is just an opportunity for us both to work together and get to know each other in a more technical way.

# Challenge

Requirements:

- VirtualBox software installed and setup in your personal computer
- A browser or Git client to fork and update this repository
- A text editor

## Installing Windows on a Virtual Machine

Download a Windows 10 ISO image from [here](https://www.microsoft.com/pt-pt/software-download/windows10) and install it on a virtual machine, using VirtualBox. You don't need to authorize that copy. Add screenshots of Windows running inside VirtualBox as proof that you have completed this task. Just add them under the `screenshots/` folder in your fork of this repository.

## Configuring static IPs on a server

Download a Ubuntu server image from [here](https://www.ubuntu.com/download/server/thank-you?version=16.04.3&architecture=amd64) and install it on a virtual machine, using VirtualBox. Afterwards, configure the Ubuntu Server installation with a static IP (use 192.168.100.1 for the IP and other random values for other configurations you might need). Your installation will stop having access to the Internet, that's intended. Add the configuration files you have changed to the `config/` folder in your fork of this repository as proof of your work.

## Plan a Zoom Room

At Unbabel, we make heavy use of the [Zoom](https://zoom.us) conferencing software. One of the tasks you will be responsible for performing is troubleshooting and setting up Zoom rooms.

Please write a short essay on what you would need to setup a Zoom Room at a new office location:

 - Shopping list
 - Software needed
 - Step-by-step configuration guide